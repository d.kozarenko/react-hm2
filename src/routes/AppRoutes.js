import React from "react";
import { Route, Switch } from "react-router-dom";
import Cart from "../pages/Cart/Cart.js";
import Favorites from "../pages/Favorites/Favorites.js";
import GoodsList from "../components/GoodsList/GoodsList.js";

const AppRoutes = ({
  cart,
  favorites,
  goods,
  switchPurchaseModal,
  switchDeleteModal,
  addToFavorites,
}) => {
  return (
    <Switch>
      <Route exact path="/">
        <GoodsList
          cart={cart}
          favorites={favorites}
          switchPurchaseModal={switchPurchaseModal}
          addToFavorites={addToFavorites}
          goods={goods}
        />
      </Route>
      <Route exact path="/favorites">
        <Favorites
          cart={cart}
          favorites={favorites}
          switchPurchaseModal={switchPurchaseModal}
          addToFavorites={addToFavorites}
          goods={goods}
        />
      </Route>
      <Route exact path="/cart">
        <Cart
          cart={cart}
          favorites={favorites}
          switchDeleteModal={switchDeleteModal}
          addToFavorites={addToFavorites}
          goods={goods}
        />
      </Route>
    </Switch>
  );
};
export default AppRoutes;
