import styled from "styled-components";

export const AppWrapper = styled.div`
  width: 1000px;
  margin: 0 auto;
`;

export const BtnsWrapper = styled.div`
  padding: 0 145px 24px;
`;
