import React from "react";
import Product from "../Product/Product.js";
import { ProductList } from "./GoodsList-styles";
import PropTypes from "prop-types";

const GoodsList = ({
  goods,
  switchPurchaseModal,
  cart,
  addToFavorites,
  favorites,
}) => {
  const goodsArr = goods.map((p) => (
    <Product
      handleClick={switchPurchaseModal}
      addToFavorites={addToFavorites}
      favorites={favorites}
      cart={cart}
      id={p.id}
      key={p.id}
      product={p}
      delBtn={false}
    />
  ));

  return <ProductList>{goodsArr}</ProductList>;
};
export default GoodsList;

GoodsList.propTypes = {
  goods: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
  favorites: PropTypes.array.isRequired,
  handleClick: PropTypes.func,
  addToFavorites: PropTypes.func.isRequired,
};
