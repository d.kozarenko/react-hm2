import React from "react";
import Svg from "../Svg/Svg.js";
import Button from "../Button/Button.js";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import {
  ProductItem,
  Img,
  DefaultLink,
  TextLink,
  Color,
  Price,
  PurchaseWrapper,
  Header,
  Body,
  Footer,
} from "./Product-styles.js";

const linkStyles = {
  textDecoration: "none",
  fontWeight: "600",
  color: "green",
  fontSize: "20px",
  margin: "0px 5px 0 0 ",
  borderBottom: "1px dotted green",
};

const Product = ({
  product,
  id,
  cart,
  handleClick,
  favorites,
  delBtn,
  addToFavorites,
}) => {
  const fill = favorites.find((el) => el === id) ? "#ffa500" : "#fff";
  return (
    <ProductItem id={id}>
      <Header>
        {!delBtn && (
          <Svg
            fill={fill}
            onClick={() => {
              addToFavorites(id);
            }}
          />
        )}
        {delBtn && (
          <Button
            bgColor={"transparent"}
            color={"black"}
            margin={"0 4px 0 0"}
            text={"x"}
            fontSize={"20px"}
            handleClick={() => {
              handleClick();
              document.getElementById(`${id}`).classList.add("activeCard");
            }}
          ></Button>
        )}
      </Header>
      <Body>
        <DefaultLink>
          <Img src={product.src}></Img>
        </DefaultLink>
      </Body>
      <Footer>
        <TextLink>{product.name}</TextLink>
        <Color>Цвет: {product.color}</Color>
        <PurchaseWrapper>
          <Price>{product.price}$</Price>
          {/* когда товара нет в локалсторедже (страница товаров) */}
          {!cart.find((el) => el === id) && (
            <Button
              padding={"10px"}
              bgColor={"#ADD8E6"}
              margin={"0 5px 0 5px"}
              text={"Купить"}
              className={`purchaseBtn`}
              handleClick={() => {
                handleClick();
                document.getElementById(`${id}`).classList.add("activeCard");
              }}
            />
          )}
          {/* когда товар в локалсторедже, показывается надпись "в корзине" для перехода в корзину (страница товаров) */}
          {cart.find((el) => el === id) && !delBtn && (
            <Link to="/cart" style={linkStyles}>
              В корзине
            </Link>
          )}
          {/* когда товар в локалсторедже (страница корзины) */}
          {cart.find((el) => el === id) && delBtn && (
            <Button
              padding={"10px"}
              bgColor={"#ADD8E6"}
              margin={"0 5px 0 5px"}
              text={"Удалить"}
              className={`purchaseBtn`}
              handleClick={() => {
                handleClick();
                document.getElementById(`${id}`).classList.add("activeCard");
              }}
            />
          )}
        </PurchaseWrapper>
      </Footer>
    </ProductItem>
  );
};

export default Product;

Product.propTypes = {
  product: PropTypes.object.isRequired,
  id: PropTypes.number.isRequired,
  cart: PropTypes.array.isRequired,
  favorites: PropTypes.array.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  delBtn: PropTypes.bool,
};
