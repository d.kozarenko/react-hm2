import React from "react";
import { CartMessage, CardsList, CartWrapper } from "./Cart-styles";
import Product from "../../components/Product/Product";

const Cart = ({
  cart,
  favorites,
  goods,
  switchDeleteModal,
  addToFavorites,
}) => {
  const cartArr = [];

  goods.forEach((g) => {
    if (cart.includes(g.id)) {
      cartArr.push(g);
    }
  });

  const cartList = cartArr.map((c) => (
    <Product
      handleClick={switchDeleteModal}
      addToFavorites={addToFavorites}
      favorites={favorites}
      cart={cart}
      id={c.id}
      key={c.id}
      product={c}
      delBtn
    />
  ));

  return (
    <CartWrapper>
      {cart.length === 0 && <CartMessage>В корзине пока пусто</CartMessage>}
      {cart.length !== 0 && <CardsList>{cartList}</CardsList>}
    </CartWrapper>
  );
};

export default Cart;
